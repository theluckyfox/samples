# CODE SAMPLES #

### opendoor ###

Plugin extends functionality of Pinpoint Booking System. Allows to create booking calendar.

### wp_compare_theme ###

Extends Comapre+ plugin which allows to compare different products from merchant feeds.
Allows to manage comparable product attributes. Working whith dependent theme.