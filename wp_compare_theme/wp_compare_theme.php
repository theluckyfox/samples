<?php
/**
 * Part of functions.php fils from compare theme.
 *
 * These functions extends basic wp functinality and allows to interact with Compae+ plugin
 * and get extended products attributes and product reviews.
 */

/**
 * function: aw_get_product_merchants
 * 
 * Returns an array of merchants attached to main product
 * 
 * @param int $post_id
 * @return object
 */
function aw_get_product_merchants($post_id = ""){
    global $wpdb;
    if( $post_id == ""){
        $post_id = get_the_id();
    }
    $q = "SELECT pm.*, p.* FROM ".$wpdb->prefix."pc_products_relationships pr, ".$wpdb->prefix."pc_products_merchants pm, ".$wpdb->prefix."pc_products p WHERE pm.slug = p.id_merchant AND p.id_product = pr.id_product AND pr.wp_post_id = '".get_the_id()."' ORDER BY p.price ASC";
    $merchants = $wpdb->get_results($q);
    return $merchants;
}


/**
 * function: aw_excerpt_more
 * 
 * Greturn Excerpt more string
 * 
 * @param string $more
 * 
 * @return string
 * 
 */
function aw_excerpt_more($more) {
    return "...";
}
add_filter('excerpt_more', 'aw_excerpt_more');

/**
 *
 * function: register_review_gallery
 * 
 * Assigning "featured galleries" to reviews "post_type"
 *
 */
function register_review_gallery(){
    if( function_exists( 'fg_register_metabox' ) ){
        add_filter('fg_post_types', function( $post_types ) {
            //array_push( $post_types, 'review' ); 
            return array( 'review' ); //$post_types;
        } );
        
        function show_fg_sidebar( $show_sidebar ) {
            return true; // ($show_sidebar comes in a false)
        } 
        add_filter( 'fg_show_sidebar', function( $show_sidebar ) {
            return true;
        }  );
    }
    else{
        add_action( 'admin_notices', function(){
            echo '<div id="message" class="notice notice-warning"><p>Plugin "Featured Galleries" required!</p></div>';
        } );
    }
}
add_action( 'after_setup_theme', 'register_review_gallery' );

add_action( 'init', function(){
    register_sidebar( array(
        'name' => 'Reviews',
        'id' => 'reviews',
        'description' => 'Sidebar for single review page',
        'before_widget' => '<div>',
        'after_widget'  => "</div>\n",
    ) );

    /**
     * dynamic sidebar for reviews category page
     */
    register_sidebar( array(
        'name' => __('Review category'),
        'id' => 'review_category',
        'description' => __('Sidebar for review category page'),
        'before_widget' => '<div>',
        'after_widget'  => "</div>\n",
    ) );
} );

/**
 * Random posts
 */
function wp_rand_posts( $trem_id ) { 
 
    $args = array(
        'post_type' => 'review',
        'orderby'   => 'rand',
        'posts_per_page' => 4, 
        'tax_query' => array(
            'taxonomy' => 'revcat',
            'terms' => ( int ) $trem_id,
        ),
    );
     
    $the_query = new WP_Query( $args );
     
    if ( $the_query->have_posts() ) {
     
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            get_template_part( 'tpl_parts/related-review-card' );
        }

        /* Restore original Post Data */
        wp_reset_postdata();
    }
     
    return $string; 
}

if( wp_doing_ajax() ){
    add_action('wp_ajax_remove_from_compare', 'ajax_remove_from_compare');
    add_action('wp_ajax_nopriv_remove_from_compare', 'ajax_remove_from_compare');
}


/**
 * removing the item from compared products by EAN
 */
function ajax_remove_from_compare(){
    //var_dump( $_POST['d'] );
    $d = sanitize_text_field( $_POST['d'] );

    foreach( $_SESSION['compared_products_list'] as $index => $product ){
        if( $product->ean == $d ){
            array_splice( $_SESSION['compared_products_list'], $index, 1);
        }
    }

    wp_die();
} 

add_action( 'wp_enqueue_scripts', 'aj_data', 99 );
function aj_data(){

    wp_localize_script('jquery', 'ajurl', 
        array(
            'url' => admin_url('admin-ajax.php')
        )
    );  

}

/**
 * retrieving list of all merchants / retailers
 */
function get_merchants_list(){
    global $wpdb;
    $q = "SELECT * FROM ".$wpdb->prefix."pc_products_merchants pm";
    return $wpdb->get_results($q);
}

/**
* Ajax from admin - reviews
*/

function ajax_search_product(){

if( isset( $_POST['product'] ) &&  !empty( $_POST['product'] ) ){

    $search_string = sanitize_text_field( $_POST['product'] );
    global $wpdb;
}


$sites = get_sites();
$sites_count = count( $sites );
$q1 = '';

$itemsCount = 10;

$products = array();
$product_map = array();
$reviews = array();
$review_map = array();

 foreach ( $sites as $blog ): switch_to_blog($blog->blog_id); 
     
        $qProducts = "SELECT prod.ean, prod.feed_product_name, prod.feed_product_image, post.ID  
            FROM 
                {$wpdb->prefix}posts AS post, 
                {$wpdb->prefix}pc_products AS prod, 
                {$wpdb->prefix}pc_products_relationships AS pr
            WHERE
                post.ID = pr.wp_post_id
            AND
                pr.id_product = prod.id_product
            AND 
            ( post.post_title LIKE '%{$search_string}%' 
              OR 
              prod.ean LIKE '%{$search_string}%' ) 
            GROUP BY pr.wp_post_id
            LIMIT 0,{$itemsCount}";

        $p = $wpdb->get_results( $qProducts );
        if( ( !is_null($p) ) && ( count( $p ) > 0 ) ){
            foreach( $p as $product ){
                if($product->ean != '' or $product->ean != null) {
                    $product_map[] = array(
                        'image' => $product->feed_product_image,
                        'ean' => $product->ean,
                        'name' => $product->feed_product_name,
                        'link' => wp_get_shortlink($product->ID),
                    );
                }
				// echo wp_get_shortlink( $product->ID );
            }
        }   

 endforeach;

 restore_current_blog();

if (count($product_map) > 0) {
    $response = '<ul>';
    foreach( $product_map as $index => $product ){
       $response .= '<li class="product_hint" data-id="'.$product['ean'].'" data-url="'.$product['link'] .'">
                    <a href="'.$product['link'].'">
                        <div>
                            <img src="'.$product['image'].'" alt="image"/>
                        </div>'.
                        '<div><p class="p-ean">'.$product['ean']. ' | </p></div>
                        <div><p class="product_name">'.$product['name'].'</p></div>        
                        </a>
                    </li>';
                    if($index >= $itemsCount) {
                        break;
                    }
    }
    $response .= '</ul>';
} else {
    $response = '<ul style="width: 310px;">
                    <li style="margin: 0 auto;">
                        <p style="padding: 8px 0px 0px 5px; margin: 0;">
                            In this category there is no such product
                        </p>
                    </li>
                  </ul>';
} 
echo $response;

wp_die();
};


/**
 * getting products by name or ean
 */
function ajax_compare_search() {
    global $wpdb;
    $search_string = $_POST['search'];

    $qProducts = "SELECT prod.ean, prod.feed_product_name, prod.feed_product_image, post.ID  
            FROM 
                {$wpdb->prefix}posts AS post, 
                {$wpdb->prefix}pc_products AS prod, 
                {$wpdb->prefix}pc_products_relationships AS pr
            WHERE
                post.ID = pr.wp_post_id
            AND
                pr.id_product = prod.id_product
            AND 
            ( post.post_title LIKE '%{$search_string}%' 
              OR 
              prod.ean LIKE '%{$search_string}%' )
            AND prod.ean != '' 
            GROUP BY pr.wp_post_id
            LIMIT 0,10";

    $response['products'] = $wpdb->get_results($qProducts);
    if ($response['products'] == null) {
        $response['status'] = 0;
    } else {
        $response['status'] = 1;
    }
    echo json_encode($response);

    wp_die();
}

add_action('wp_ajax_sv_compare_search', 'ajax_compare_search');
add_action('wp_ajax_nopriv_sv_compare_search', 'ajax_compare_search');


add_action('wp_ajax_nopriv_sv_admin_review_search', "ajax_search_product");
add_action('wp_ajax_sv_admin_review_search', "ajax_search_product");

add_action( 'admin_enqueue_scripts', 'sv_add_product_css' );


/**
 * getting offered products on single review page
 * 
 * @param int $ean
 *
 * @return array | null
 */
function get_offered_product( $ean ){
    global $wpdb;

    $sites = get_sites();
    $sites_count = count( $sites );

    $products = array();
    $product_map = array();

    foreach ( $sites as $blog ): switch_to_blog($blog->blog_id);

            $qProducts = "SELECT 
                    prod.ean, 
                    prod.feed_product_name, 
                    prod.feed_product_image, 
                    post.ID, 
                    ( SELECT COUNT(*) FROM `{$wpdb->prefix}pc_products` WHERE ean LIKE '{$ean}' ) AS retailers_count,
                    ( SELECT MIN( price ) FROM `{$wpdb->prefix}pc_products` WHERE ean LIKE '{$ean}' ) AS price 
                FROM 
                    {$wpdb->prefix}posts AS post, 
                    {$wpdb->prefix}pc_products AS prod, 
                    {$wpdb->prefix}pc_products_relationships AS pr
                WHERE
                    post.ID = pr.wp_post_id
                AND
                    pr.id_product = prod.id_product
                AND prod.ean IS NOT NULL
                AND prod.ean != ''
                AND prod.ean LIKE '{$ean}%'
                GROUP BY prod.ean
                LIMIT 0,1";

            $p = $wpdb->get_row( $qProducts );

             $brand = wp_get_post_terms($p->ID, 'product_brand');

            if( ( !is_null($p) ) && ( count( $p ) > 0 ) ){
                
                $product_map[] = array(
                    'image' => $p->feed_product_image,
                    'ean' => $p->ean,
                    'name' => $p->feed_product_name,
                    'link' => wp_get_shortlink( $p->ID ),
                    'retailers' => $p->retailers_count,
                    'price' => $p->price,
                    'bid' => ( $blog->blog_id == 1 ) ? '' : $blog->blog_id.'_',
                    'brand' => $brand[0]->name,
                );
            }
    restore_current_blog();
    
    endforeach;

   return $product_map;
}
function sv_add_product_css() {
    wp_enqueue_style( 'prefix-style', get_template_directory_uri().'/css/admin/sv_product_admin.css' );
}

/**
* Ajax from products - shopinglist
*/
function add_in_shoplist() {
    if(wp_auth_check()){
        $user = wp_get_current_user();
            
        $query = new WP_Query(array('post_author'   => $user->ID, 
                                    'post_type'     => "shoppinglist"));
        $a=array();
        foreach ($query as $key => $value) {
            $a .= $value->ID;
        }
        echo($a);
        die();
        
    }
}

add_action('wp_ajax_nopriv_sv_shoplist', "add_in_shoplist");
add_action('wp_ajax_sv_shoplist', "add_in_shoplist");

/**
 * getting first {count} words from ncoming line
 *
 * @param string $sentence
 * @param int $count of sliced wordss
 */
function get_words($sentence, $count = 10) {

  return implode(' ', array_slice(explode(' ', $sentence, $count+1), 0, $count));
}

/**
 * getting blogs and categores from this blog for displaying related products
 */
add_action( 'wp_ajax_aj_review_rel_products', 'aj_review_rel_products' );

function aj_review_rel_products(){
    if( current_user_can('manage_options') ){

        $data = '';

        if( isset( $_GET['part'] ) ){
            switch( $_GET['part'] ){
                case 'b':
                    $b = get_sites();
                    $d = array();

                    $d['current']['blog'] = get_field('blog', $_GET['pid']);
                    $d['current']['category'] = get_field('category', $_GET['pid']);

                    foreach( $b as $site ){
                        $d['blog_list'][$site->blog_id] = $site->domain.$site->path;
                    }

                    switch_to_blog( $d['current']['blog']);
                    $t = get_terms( 'product_category' );
                    restore_current_blog();

                    foreach( $t as $term ){
                        $d['category_list'][$term->term_id] = $term->name;
                    }

                    $data = json_encode( $d );
                    break;
                case 'c':
                    $d = array();
                    if( isset( $_GET[ 'bid' ] ) ){
                        $bid = sanitize_text_field( $_GET['bid'] );
                    }
                    else{
                        $bid = 1;
                    }

                    switch_to_blog( $bid );

                    $t = get_terms( 'product_category' );

                    restore_current_blog();

                    foreach( $t as $term ){
                        $d['list'][$term->term_id] = $term->name;
                    }
                    $data = json_encode( $d );
                    break; 
            }
        }

        echo $data;
    }

    wp_die();
}

function get_product_by_blog_id($blog_id, $term_id ){
    global $wpdb;

    $product_map = array();

    $qPost = array(
        'post_type' => 'product',
        'numberposts' => 15,
        'post_status' => 'publish',
        'orderby' => 'rand',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_category',
                'field'    => 'term_id',
                'terms'    => $term_id
            ),
        )
    );

    switch_to_blog($blog_id);

    $products = get_posts( $qPost );
    foreach ($products as $product) {
        $qProducts = "SELECT  
                    prod.ean, 
                    prod.feed_product_name, 
                    prod.feed_product_image, 
                    post.ID,
                    ( SELECT COUNT(*) FROM {$wpdb->prefix}pc_products WHERE ean LIKE prod.ean ) AS retailers_count,
                    ( SELECT MIN( price ) FROM {$wpdb->prefix}pc_products WHERE ean LIKE prod.ean ) AS price 
            FROM 
                    {$wpdb->prefix}posts AS post, 
                    {$wpdb->prefix}pc_products AS prod, 
                    {$wpdb->prefix}pc_products_relationships AS pr
            WHERE post.ID = pr.wp_post_id
            AND prod.ean IS NOT NULL
            AND prod.ean != ''
            AND pr.id_product = prod.id_product
            AND post.id = {$product->ID}
            LIMIT 0, 1";

        $p = $wpdb->get_row($qProducts);
        $brand = wp_get_post_terms($p->ID, 'product_brand');

        if ((!is_null($p)) && (count($p) > 0)) {

            $product_map[] = array(
                    'id' => $p->ID,
                'image' => $p->feed_product_image,
                'ean' => $p->ean,
                'name' => $p->feed_product_name,
                'link' => wp_get_shortlink($p->ID),
                // 'link' => wp_get_shortlink($p->ID),
                'retailers' => $p->retailers_count,
                'price' => $p->price,
                'brand' => $brand[0]->name,
            );
        }
    }
    restore_current_blog();
    return $product_map;
}

/**
 * getting all not empty extra product attributes
 * 
 * @param int $post_id - id of the product post
 * 
 * @return array / false
 */
function get_product_attributes( $post_id ){
    global $wpdb, $aw_theme_options;

    $adv_options = get_option( 'cp_parser_extra_options' );

    $adv_options_excluded = $aw_theme_options['tz_advanced_options_excluded'];

    /**
     * parse exclusion and take into account excluded options
     */

    $adv_options_excluded = cp_parse_string( $adv_options_excluded );

    if( !$adv_options ){
        return false;
    }

    if( !$adv_options_excluded ){
        $adv_options_excluded = array();
    }

    $fields_list = array();

    foreach( $adv_options as $index => $adv_option ){
        $fields_list[] = '`p`.`'.$adv_option[1].'`';
    }

    $fields_list = implode( ', ', $fields_list );

    $q = "SELECT 
                {$fields_list} 
            FROM 
                {$wpdb->prefix}pc_products AS p,
                {$wpdb->prefix}pc_products_relationships AS pr
            WHERE
                `p`.`id_product` = `pr`.`id_product`
            AND
                `pr`.`wp_post_id` = {$post_id}";

    $product_raw_data = $wpdb->get_results( $q, ARRAY_A );

    if( is_null( $product_raw_data ) ){
        return false;
    }

    $data = array();

    foreach( $product_raw_data as $product ){

        foreach( $adv_options as $index => $adv_option) {
            if( array_key_exists( $adv_option[1], $product ) && ( !is_null( $product[ $adv_option[1] ] ) ) && ( !empty( $product[ $adv_option[1] ] ) ) && ( !in_array( $adv_option[1], $adv_options_excluded ) ) ){
                $data[ $adv_option[1] ] = array( $adv_option[0], $product[ $adv_option[1] ] );
                continue;
            }
        }

    }

    return $data;
}

/**
 * parse comma-separated string of options to be excluded
 * 
 * @param string $string
 * 
 * @return array | false
 */
function cp_parse_string( $string ){
    if( empty( $string ) ){
        return false;
    }

    $raw = explode(',', $string);

    $data = array_map( function( $s ){
        return trim( $s );
    }, $raw );

    return $data;
}


/**
 * getting list of shopping lists
 */
function get_shopping_lists(){
    /**
     *  current user id
     */
    $uid = get_current_user_id();

    /**
     * finish work if incorrect user
     */
    if( $uid == 0 ){
        wp_die();
    }

    /**
     * searching for user's shooping lists
     */
    $args = array(
        'post_type' => array( 'shoppinglist' ),
        'author' => $uid,
    );

    $sl = new WP_Query( $args );

    if( !is_null( $sl ) ){

        foreach( $sl->posts as $post ){
            echo '<p data-id="'.$post->ID.'">'.$post->post_title.'</p>';
        }
    }
    wp_die();
}

add_action( 'wp_ajax_aj_get_shopping-lists', 'get_shopping_lists' );

/**
 * adding selected product to the shopping list
 */
function add_in_shopping_list(){

    if( isset( $_POST['list'] ) && !empty( $_POST['list'] ) && isset( $_POST['ean'] ) && !empty( $_POST['ean'] ) ){
        $uid = get_current_user_id();
        $ean = sanitize_text_field( $_POST['ean'] );
        $list = sanitize_text_field( $_POST['list'] );

        if( $uid == 0 ){
            wp_die();
        }

        $post = get_post( $list );

        if( !is_null( $post ) ){
            if( $post->post_author == $uid ){
                $eans = json_decode( trim( stripcslashes( $post->post_content ) ) );

                if( $eans ){
                    if( in_array( $ean, $eans ) ){
                        array_splice($eans, array_search( $ean, $eans), 1);
                    }
                    else{
                        array_push( $eans, $ean );
                    }
                }
                else{
                    $eans = array( $ean );
                }

                $args = array(
                    'ID' => (int)$post->ID,
                    'post_content' => esc_sql( json_encode( $eans ) ),
                );

                if( wp_update_post( $args ) ){
                    echo esc_sql( json_encode( $eans ) );
                }
            }
        }
    }
    wp_die();
}

add_action( 'wp_ajax_add_in_shopping_list', 'add_in_shopping_list' );

/**
 * creating new shopping list with required name
 */
function create_shopping_list(){
    $uid = get_current_user_id();
    if( $uid == 0 ){
        wp_die();
    }

    if( isset( $_POST['list_name'] ) && !empty( $_POST['list_name'] ) ){
        $list_name = wp_strip_all_tags( sanitize_text_field( $_POST['list_name'] ) );

        $args = array(
            'post_type' => 'shoppinglist',
            'post_title' => $list_name,
            'post_status' => 'publish',
            'post_author' => $uid,
        );

        $post_id = wp_insert_post( $args );

        if( ( $post_id != 0 ) && ( !is_wp_error( $post_id ) ) ){
            echo '<p data-id="'.$post_id.'">'.$list_name.'</p>';
        }
    }

    wp_die();
}

add_action( 'wp_ajax_create_shopping_list', 'create_shopping_list' );

/**
 * applying unit map to option
 * add scale unit if exists to option value
 * 
 * @param string $option - the name of option column from db
 * @param string $value
 *
 * @return string - value + scale unit | value
 */
function apply_units_map( $option, $value, $merge = false ){
    $units_map = array(
        'calories' => __( 'kcal' ),
        'kalories' => __( 'kcal' ),
        'salt' => __( 'g' ),
        'fibres' => __( 'g' ),
        'protein' => __( 'g' ),
        'fat' => __( 'g' ),
        'saturated_fat' => __( 'g' ),
        'carbs' => __( 'g' ),
        'sugar' => __( 'g' ),
        'alcohol_percentage' => '%',
    );

    $value_has_units = function( $value ){
        return preg_match( '/\%$/', $value );
    };

    if( $value_has_units( $value ) ){
        return $value;
    }

    if( array_key_exists($option, $units_map) ){
        return $merge ? $value.' '.$units_map[ $option ] : $units_map[ $option ];
    }
    else{
        return $value;
    }

}

/**
 * getting the price for unit of product ( CHF 2.10 / 100g )
 *
 * @param object $product
 * 
 * @return string - formatted price for unit
 */
function get_product_unit_price( $product ){
    if( !is_object( $product ) ){
        return false;
    }

    if( empty( $product->size ) || empty( $product->price ) ){
        return false;
    }

    $is_compound_size = function( $size ){
        return preg_match('/[a-zA-Z]+$/', $size);
    };

    if( $is_compound_size( $product->size ) ){
        preg_match( '/^\d+/', $product->size, $matches );
        $size = $matches[0];

        preg_match( '/[a-zA-z]+$/', $product->size, $matches );
        $unit = $matches[0];
    }
    else{
        $unit = $product->unit;
        $size = floatval( $product->size );
    }

    $price = floatval( $product->price );

    $ratio_map = array(
        'l' => 1,
        'L' => 1,
        'ml' => 100,
        'ML' => 100,
        'Ml' => 100,
        'cl' => 10,
        'CL' => 10,
        'Cl' => 10,
        'Kg' => 1,
        'KG' => 1,
        'g' => 100,
        'G' => 100,
        'M' => 1,
        'm' => 1,
        'cm' => 10,
        'Cm' => 10,
        'CM' => 10,
    );

    if( array_key_exists($unit, $ratio_map) ){
        $the_price = $price / $size * $ratio_map[ $unit ];
        return aw_the_formated_price( $the_price, true ).' / '.$ratio_map[ $unit ].' '.$unit;
    }
    
    return false;
    
}

?>