/**
 * managing shopping lists
 *
 * 1 - creating new shopping list
 * 2 - getting all available lists of user
 * 3 - puting product to selected list
 */

jQuery( document ).ready( function(){
	jQuery( '.product-view' ).append( '<div class=\'shopping_lists\' style=\'width: 198px; min-height: 200px; position: absolute; z-index: 1000; left: -15px; top: 115px;\'><div class="sl-controls"><input type="text" class="new-list-name" /><button class="add-shopping-list-btn">+</button></div><div class="sl-body"></div></div>' );
	jQuery( '.shopping_lists' ).css("display", "none");

	var active_product = {
		ean: '',
		list: '',
	};

	/**
	 * getting all available shopping lists
	 */
	jQuery(".add-to-shopping-list-btn").on('click', function(){

		// jQuery( '.shopping_lists' ).hide();

		var product_card = this;

		active_product.ean = jQuery( this ).data( 'id' );

		var aj_action = 'aj_get_shopping-lists';

		var sl = jQuery( this ).parent().parent().find( '.shopping_lists .sl-body' ).first();

		if( sl.parent().css( 'display' ) == 'none' ){

			jQuery.ajax({
				url: ajurl.url,
				type: 'POST',
				data: {
					action: aj_action,
				},
			}).done(function( response ) {
								
				sl.empty().append( response );

				sl.parent().fadeIn( 'fast' );
			});
		}
		else{
			sl.parent().fadeOut( 'fast' );
		}

		console.log( sl );

	})

	/**
	 * put selected product to the selected shopping list
	 */
	jQuery( document ).on("click", ".shopping_lists p", function(){

		active_product.list = jQuery( this ).data( 'id' );

		var aj_action = 'add_in_shopping_list';

		jQuery.ajax({
			url: ajurl.url,
			type: 'POST',
			data: {
				action: aj_action,
				list: active_product.list,
				ean: active_product.ean,
			},
		}).done(function( r ) {
			 console.log( r );
		
		});

	});

	/**
	 * creating new shopping list
	 */
	jQuery( document ).on( 'click', '.add-shopping-list-btn', function(){

		var el = jQuery( this );

		var aj_action = 'create_shopping_list';

		var list_name = jQuery( this ).parent().find( '.new-list-name' ).first().val();

		var list_name_mask = /^\w+\s?$/;



		if( ( list_name.length > 3 ) ){
			jQuery.ajax( {
				url: ajurl.url,
				type: 'post',
				data: {
					action: aj_action,
					list_name: list_name,
				},
			} ).done( function( r ){
				el.parent().parent().find( '.sl-body' ).first().append( r );
				el.parent().find( '.new-list-name' ).val( '' );
			} );
		}

	} );

} );