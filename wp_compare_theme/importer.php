<?php
/**
 * Importing required data from custom database to wordpress database
 * using categories mapping, users mapping and custom fields mapping.
 *
 * Import splited on few steps. Each step imports assigned to it data.
 * Import of main articles is iterated. There are 100 articles imports 
 * per one iteration.
 * 
 * The source of articles is external database. 
 */

/**
 * importing users ( step 1 )
 */
function import_users(){
	$publishers = get_review_publishers();

	$publishers_mapping = array();
	
	foreach( $publishers as $publisher ) {
		$userdata = array(
			'user_pass'       => md5( time() ),
			'user_login'      => 'Publisher'.$publisher->id,
			'display_name'    => $publisher->name,
			'role' 			  => 'author',
		);

		$publisher_id = wp_insert_user( $userdata );
		if( !is_wp_error( $publisher_id ) ) {
			$publishers_mapping[ $publisher->id ] = $publisher_id;
		}
	}

	update_option( 'reviews_import_publisher_mapping', $publishers_mapping, false );
}

/**
 * importing categories ( step 2 )
 *
 * @param string $taxonomy - the taxonomy for new items
 */
function import_categories(){
	$cats = get_all_categories( $taxonomy = 'revcat' );

	$category_mapping = array();

	foreach( $cats as $cat ){
		$cat_args = array(
			'cat_name' => $cat->name,
			'category_description' => $cat->description,
			'category_nicename' => $cat->alias,
			'taxonomy' => $taxonomy,
		);

		$cat_id = wp_insert_category( $cat_args );
		if( !is_wp_error( $cat_id ) ) {
			$category_mapping[ $cat->id ] = $cat_id;
		}
	}
	update_option( 'reviews_import_category_mapping', $category_mapping, false );
}

/**
 * importing rerviews ( step 3 )
 */
function import_reviews(){

	$posttype = 'review';
	$publishers_mapping = get_option( 'reviews_import_publisher_mapping' );
	$category_mapping = get_option( 'reviews_import_category_mapping' );

	$reviews = get_reviews();

	/**
	 * array with inserted post id and review id in native table
	 */
	$reviews_map = array();

	/**
	 * creating new reviews from imported
	 */
	foreach( $reviews as $index => $review ){

		if ( !is_object( $publishers_mapping[ $review->review_publisher_id ] ) ){
			$post_author = $publishers_mapping[ $review->review_publisher_id ];
		}
		else{
			$post_author = 1;
		}

		$post_data = array(
			'post_title'    => wp_strip_all_tags( $review->title ),
			'post_content'  => $review->body_text,
			'post_status'   => 'publish',
			'post_author'   => $post_author,
			'post_excerpt' 	=> $review->lead_text,
			'post_type'		=> $posttype,
			'post_name'		=> $review->slug,
			'tax_input'     => array( $taxonomy => array( $category_mapping[ $review->category_id ] ) ),
			'post_date'     => $review->review_date,
		);

		$post_id = wp_insert_post( $post_data );

		if( ( $post_id != 0 ) && ( !is_wp_error( $post_id ) ) ){
			
			/**
			 * insert source_url value to custom field
			 */
			if( function_exists( 'update_field' ) ){
				update_field( 'review_source_url', $review->source_url, $post_id );
				update_field( 'review_source_title', $review->source_url, $post_id );
			}


			$reviews_map[ $review->id ] = $post_id;

			if( function_exists( 'add_row' ) ){
				$products = get_reviewed_products( $review->id );

				/**
				 * inserting products in custom fields
				 */
				foreach( $products as $idx => $product ){

					/**
					 * adding row to repeater ( products )
					 */
					$row = array(
						'search_string'	=> $product->name,
						'score'	=> $product->ratingtext,
						'rating'	=> $product->rating,
						'product_description'	=> $product->description,
						'pro_arguments'	=> $product->plus,
						'con_arguments'	=> $product->minus,
						'ean'	=> $product->ean,
						'product_image' => $product->image,
						'merchant_link' => $product->deeplink,
					);

					$i = add_row('review-products-list', $row, $post_id);
				}

				$criterias = get_review_criterias( $review->id );

				/**
				 * inserting test criterias to custom fields
				 */
				foreach( $criterias as $idx => $criteria ){
					/**
					 * adding row to repeater ( criterias )
					 */
					$row = array(
						'criteria_title'	=> $criteria->name,
						'description'	=> $criteria->description,
						'scale'	=> $criteria->scale,
					);

					$i = add_row('test_criteria', $row, $post_id);	
				}
			}
		}

		update_option( 'reviews_import_review_mapping', $reviews_map, false );

	}
}

/**
 * getting reviews
 */
function get_reviews(){
	global $wpdb2;

	$r = $wpdb2->get_results("SELECT * FROM `tb_review` LIMIT 0, 1000");
	return $r;
}

/**
 * getting products from review
 *
 * available fields in response:
 * - rating
 * - description
 * - plus
 * - minus
 * - ratingtext
 * - name
 * - ean
 * - deeplink
 * 
 * @param int $review_id - id of review which has some products
 *
 * @return array | null
 */
function get_reviewed_products( $review_id ){
	global $wpdb2;

	$products = $wpdb2->get_results( "SELECT 
		`tb_product_review`.`rating` AS rating, 
		`tb_product_review`.`description` AS description, 
		`tb_product_review`.`plus` AS plus, 
		`tb_product_review`.`minus` AS minus, 
		`tb_product_review`.`ratingtext` AS ratingtext, 
		`tb_product`.`name` AS name, 
		`tb_product`.`ean` AS ean, 
		`tb_product`.`deep_link` AS deeplink, 
		`tb_product`.`image` AS image 
		FROM `tb_product_review`, `tb_product` 
		WHERE `tb_product_review`.`review_id` = {$review_id} 
		AND `tb_product`.`id` = `tb_product_review`.`product_id`" );
	return $products;
}

/**
 * getting criterias from original review
 *
 * available fields in response:
 * - name
 * - description
 * - scale
 *
 * @param int $review_id - id of review which has some criterias
 *
 * @return array | null
 */
function get_review_criterias( $review_id ){
	global $wpdb2;

	$criterias = $wpdb2->get_results( "SELECT 
		`tb_review_criteria`.`name` AS name, 
		`tb_review_criteria`.`description` AS description, 
		`tb_review_criteria`.`emphasis` AS scale 
		FROM `tb_review`, `tb_review_criteria` 
		WHERE `tb_review_criteria`.`review_id` = `tb_review`.`id` 
		AND `tb_review`.`id` = {$review_id}" );
	
	return $criterias;
}

/**
 * getting all categories
 *
 * @return array | null
 */
function get_all_categories(){
	global $wpdb2;

	$cats = $wpdb2->get_results( "SELECT * FROM `tb_category` LIMIT 0, 200" );

	return $cats;
}

/**
 * getting all review authors
 */
function get_review_publishers(){
	global $wpdb2;

	$publishers = $wpdb2->get_results( "SELECT * FROM `tb_review_publisher`" );

	return $publishers;
}

/**
 * setting up new bd connection
 * 
 * @param array $config - array with access credentials
 */
function setup_connection( $config = array( 'uname' => '', 'pass' => '', 'db_name' => '', 'host' => '' ) ){

	require_once( ABSPATH . '/wp-admin/includes/taxonomy.php');

	global $wpdb2;

	$wpdb2 = new wpdb( $config['uname'], $config['pass'], $config['db_name'], $config['host'] );

	if( !empty($wpdb2->error) ) wp_die( $wpdb2->error );

}

if( isset( $_GET['iaction'] ) ){
	$act = sanitize_text_field( $_GET['iaction'] );

	setup_connection();

	switch( $act ){
		case 'users':
			import_users();
			break;
		case 'cats':
			import_categories();
			break;
		case 'tests':
			import_reviews();
			break;
	}
}