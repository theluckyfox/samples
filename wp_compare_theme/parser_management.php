<?php 
/**
 * The functions group which extends the basic functions of Compare+ pluin
 *
 * These functions allows to add / remove / manage compare+ product attributes
 * These functions appears in admin side. Most of them assigned to ajax-hooks.
 */

/**
 * adding custom options to compare+ parser
 */
function cp_add_options(){
	if( current_user_can('manage_options') && ( isset( $_POST['bid'] ) ) ){
		/**
		 * name of option in database which store all parser extra options
		 */
		$options_holder = 'cp_parser_extra_options';

		/**
		 * errors list, contains report about existed or wrong options
		 */
		$errors = array();

		/**
		 * blog id
		 */
		$bid = sanitize_text_field( $_POST['bid'] );



		/**
		 * handle incoming data
		 */
		if( isset( $_POST[ 'fields' ] ) ){

			$raw_options = stripslashes_deep( $_POST[ 'fields' ] );

			$options = json_decode( $raw_options );

			if( !is_null( $options ) ){

				/**
				 * switching to appropriate blog
				 */
				switch_to_blog( $bid );

				/**
				 * existed options in database
				 */
				$current_options = get_option( $options_holder );
				
				foreach( $options as $index => $option ){
					/**
					 * flag which says is newly added option exists in db or not
					 */
					$already_exists = false;

					$o_label = $option[0];
					$o_name = $option[1];
					$o_val = $option[2];

					/**
					 * check if option already exists
					 */
					if( empty( $current_options ) || !is_array( $current_options ) ){
						$current_options = array();
						// $current_options[] = array( $o_label, $o_name, $o_val );


						if( ( cp_is_valid_option_name( $o_name ) ) && ( cp_create_column( $o_name, 'pc_products' ) ) && ( cp_create_column( $o_name, 'pc_parsers' ) ) && ( cp_create_column( $o_name, 'pc_products_raw' ) ) ){
							$current_options[] = array( $o_label, $o_name, $o_val );
						}
						else{
							$errors[] = array( $index, __( 'Option can\'t be saved because its name is not valid or can\'t create column with such name.', 'framework' ) );
						}
					}
					else{

						foreach( $current_options as $c_option ){
							if( $c_option[1] == $o_name ){
								$already_exists = true;
								break;
							}							
						}

						if( $already_exists ){
							$errors[] = array( $index, 'Option with this name already exists' );
						}
						else{
							if( ( cp_is_valid_option_name( $o_name ) ) && ( cp_create_column( $o_name, 'pc_products' ) ) && ( cp_create_column( $o_name, 'pc_parsers' ) ) && ( cp_create_column( $o_name, 'pc_products_raw' ) ) ){
								$current_options[] = array( $o_label, $o_name, $o_val );
							}
							else{
								$errors[] = array( $index, __( 'Option can\'t be saved because its name is not valid or can\'t create column with such name.', 'framework' ) );
							}
						}
					}
				}
				update_option( $options_holder, $current_options, false );

				echo json_encode( $errors );

				restore_current_blog();
			}
		}
	}
	wp_die();
}

add_action( 'wp_ajax_cp_add_options', 'cp_add_options' );

/**
 * create column in table with required name
 *
 * @param string $column_name
 * @param string $table_name - the name of the table without prefix
 *
 * @return bool
 */
function cp_create_column( $column_name, $table_name = '' ){
	if( empty( $table_name ) ){
		return false;
	}

	global $wpdb;

	$q = "SHOW COLUMNS FROM `{$wpdb->dbname}`.`{$wpdb->prefix}{$table_name}`";

	$r = $wpdb->get_results( $q );

	foreach ( $r as $index => $option ) {
		if( $option->Field == $column_name ){
			return false;
		}
	}

	$q = "ALTER TABLE `{$wpdb->dbname}`.`{$wpdb->prefix}{$table_name}` ADD {$column_name} VARCHAR( 255 )";

	return $wpdb->query( $q );
}

/**
 * removing column from table with requested name
 * 
 * @param string $column_name
 * @param string $table_name - the name of the table without prefix
 *
 * @return bool
 */
function cp_remove_column( $column_name, $table_name = '' ){
	if( empty( $table_name ) ){
		return false;
	}

	global $wpdb;

	$q = "SHOW COLUMNS FROM `{$wpdb->dbname}`.`{$wpdb->prefix}{$table_name}`";

	$r = $wpdb->get_results( $q );

	foreach ( $r as $index => $option ) {
		if( $option->Field == $column_name ){
			$q = "ALTER TABLE `{$wpdb->dbname}`.`{$wpdb->prefix}{$table_name}` DROP COLUMN {$column_name}";
			return $wpdb->query( $q );
		}
	}

	return false;
}

/**
 * check the name of new option, can be only [a-zA-Z_]
 * 
 * @param string $name
 * 
 * @return bool
 */
function cp_is_valid_option_name( $name ){
	$regexp = '/\w+/';
	return preg_match( $regexp, $name);
}

/**
 * rendering parser extra options
 * 
 * @param bool $render - render or return result
 * @param array $wrapper_options
 *
 * @return string | bool
 */
function get_extra_options( $render = false, $parser_id = false, $wrapper_options = array( 'class' => 'cp-adv-option' ) ){
	$fields = get_option( 'cp_parser_extra_options' );
	
	global $wpdb, $aw_theme_options;

	/**
	 * getting theme options field which contain excluded parser fields 
	 */
	$hidden_fields_raw = $aw_theme_options[ 'tz_advanced_options_excluded' ];

	if( function_exists( 'cp_parse_string' ) ){
		$hidden_fields = cp_parse_string( $aw_theme_options['tz_advanced_options_excluded'] );
	}

	$options = array();

	if( is_numeric( $parser_id ) ){
		$q = "SELECT * FROM {$wpdb->prefix}pc_parsers WHERE id={$parser_id}";
		$data = $wpdb->get_row( $q, ARRAY_A );

		if( !is_null( $data ) ){
			foreach ( $fields as $index => $field ) {
				if( array_key_exists( $field[1], $data ) ){
					if( in_array( $field[1], $hidden_fields ) ){
						$display_status = 0;
					}
					else{
						$display_status = 1;
					}

					array_push( $options, array( $field[0], $field[1], $data[ $field[1] ], $display_status ) );
				}		
			}
		}
		else{
			$options = $fields;
		}
	}
	else{
		$options = $fields;
	}
	

	if( !$options ){
		return false;
	}

	$s = '';

	$_b = get_current_blog_id();

	foreach( $options as $option ){

		$status_class = $option[3] ? 'cp-visible' : 'cp-hidden';

		$s .= '<tr valign="top" class="'.$wrapper_options['class'].'" data-option-name="'.$option[1].'">
				<th scope="row"><label for="feed_product_weight">'.$option[0].'</label></th>
				<td><input type="text" size="75" name="adv_parser['.$option[1].']" id="'.$option[1].'" class="tipsy-tooltip" value="'.$option[2].'" original-title="Please enter the name of the field for the '.$option[0].'"><span class="cp-delete-option-btn">Delete option</span>
				<span class="toggle-option-status '.$status_class.'" data-option-name="'.$option[1].'" data-b="'.$_b.'" title="Display or hide this option on site">&nbsp;</span></td>
			</tr>';
	}

	if( !$render ){
		return $s;
	}
	else{
		echo $s;
	}

}

/**
 * deleting option from compare+ parser
 */
function cp_delete_option(){
	if( current_user_can('manage_options') && ( isset( $_POST['bid'] ) ) ){

		/**
		 * name of option in database which store all parser extra options
		 */
		$options_holder = 'cp_parser_extra_options';

		/**
		 * errors list, contains report about existed or wrong options
		 */
		$errors = array();

		/**
		 * blog id
		 */
		$bid = sanitize_text_field( $_POST['bid'] );

		/**
		 * handle incoming data
		 */
		if( isset( $_POST[ 'option_name' ] ) ){

			$option_name = sanitize_text_field( $_POST[ 'option_name' ] );

			if( !is_null( $option_name ) && cp_is_valid_option_name( $option_name ) ){

				/**
				 * switching to appropriate blog
				 */
				switch_to_blog( $bid );

				/**
				 * existed options in database
				 */
				$current_options = get_option( $options_holder );

				foreach( $current_options as $index => $option ){
					if( $option[1] == $option_name ){
						if( cp_remove_column( $option_name, 'pc_products' ) && cp_remove_column( $option_name, 'pc_parsers' ) && cp_remove_column( $option_name, 'pc_products_raw' ) ){

							array_splice( $current_options, (int)$index, 1 );
							update_option( $options_holder, $current_options, false );

							echo 'ok';
						}
						break;
					}
				}
				restore_current_blog();
			}
		}
		else{
			$errors[] = __( 'Option can\'t be deleted because its name is empty.', 'framework' );
		}
	}

	wp_die();
}

add_action( 'wp_ajax_cp_delete_option', 'cp_delete_option' );

/**
 * saving advanced parser options in Compare+
 * 
 * @param array $data
 * @param int $parser_id
 *
 * @return bool
 */
function save_advanced_options( $data, $parser_id ){
	global $wpdb;

	$q = "SHOW COLUMNS FROM `{$wpdb->dbname}`.`{$wpdb->prefix}pc_parsers`";

	$r = $wpdb->get_results( $q );

	$columns = array();
	$values = array();

	$conditions = array();

	/**
	 * advanced options configuration from wp_options
	 */
	$adv_options = get_option( 'cp_parser_extra_options' );

	/**
	 * new values for advanced options
	 */
	$patched_options = array();

	foreach( $r as $index => $option ){
		if( array_key_exists( $option->Field, $data ) ){

			/**
			 * updating options values
			 */
			foreach( $adv_options as $i => $adv_option ){
				if( $adv_option[1] == $option->Field ){
					array_push( $patched_options, array( $adv_option[0], $adv_option[1], $data[ $option->Field ] ) );
				}
			}

			$c = $option->Field.'='.'\''.$data[ $option->Field ].'\'';
			array_push( $conditions, $c );
		}
	}

	$s = implode( ', ', $conditions );
	$q = "UPDATE {$wpdb->prefix}pc_parsers SET {$s} WHERE id = {$parser_id};";

	if( $wpdb->query( $q ) ){
		update_option( 'cp_parser_extra_options', $patched_options );
	}

	return ;

}

/**
 * toggle option status
 */
function cp_toggle_option_status(){
	if( current_user_can('manage_options') && ( isset( $_POST['option'] ) ) && ( isset( $_POST['b'] ) ) && ( function_exists( 'cp_parse_string' ) ) ){

		$option = sanitize_text_field( $_POST['option'] );
		$blog_id = sanitize_text_field( $_POST['b'] );

		switch_to_blog( $blog_id );

		$options = cp_parse_string( get_option('tz_advanced_options_excluded' ) );

		if( empty( $option ) ){
			wp_die();
		}

		if( in_array( $option, $options ) ){

			array_splice($options, array_search($option, $options), 1);
		}
		else{
			array_push($options, $option);
		}

		$options_raw = implode(',', $options);
		update_option( 'tz_advanced_options_excluded', $options_raw );

		restore_current_blog();

		echo '200';
	}
	wp_die();
}

add_action( 'wp_ajax_toggle_option_status', 'cp_toggle_option_status' );