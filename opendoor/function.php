<?php
/**
 * form of authorization
 * output of the form for outpu log in and password
 * @param $display - if the line exist- then error when authorization, "1" if there is no error
 */
function open_form($display = 0){
    $form = '<form action="" method="POST" class="od-form-open" id="od-form-open">';
    $form .= '<br><input type="hidden" name="od-hidInput" value="openDoorTest" >';
    $form .= '<label> Login or Email <input type="text" name="od-form-open-name" ></label>';
    $form .= '<br><label> Password or Key <input type="password" name="od-form-open-pass" ><br><br>';
    $form .= '<input type="submit" value="Open Go" name="od-form-open-sub" class="od-button"><br><br>';
    if($display){
        $form .= '<p style="color: #c00; font-weight: bold">' . $display . '</p>';
    }
    $form .= '</form>';
    echo $form;
}

/**
 * creating the table in database 'opendoor'
 * check if there is no table in database, then the table of the plugin is created:
 * number of the calendar, name of the calendar, hrs:minutes
 * and it is filled by existing calendars of provided in plugin Pinpoint Booking System from the table dopbsp_calendars;
 * default time "0";
 */
function create_bd_opendoor(){
    global $wpdb, $tableOpenDoor;
    if ($wpdb->get_var("SHOW TABLES LIKE '$tableOpenDoor'") != $tableOpenDoor) {
        $sql = "CREATE TABLE " . $tableOpenDoor . " (
            id int  PRIMARY KEY NOT NULL AUTO_INCREMENT,
            id_calendar int DEFAULT '0' NOT NULL,
            name_calendar VARCHAR(255) NOT NULL,
            hours int DEFAULT '0' NOT NULL,
            minutes int DEFAULT '0' NOT NULL,
            date_created TIMESTAMP,
            UNIQUE KEY id (id)
	    );";
        dbDelta($sql);
        $tableCalendars = $wpdb->prefix . "dopbsp_calendars";
        $query = "SELECT id , name FROM $tableCalendars";
        $results = $wpdb->get_results( $query );
        foreach ($results as $key=>$value) {
            $query = "INSERT INTO $tableOpenDoor(id_calendar,name_calendar ) VALUES({$value->id}, '{$value->name}')";
            $wpdb->get_results($query);
        }
    }
}

/**
 * the outputting of data into admin panel by existing calendars.
 *it updates the page of the plugin opendoor when the user go to the page of the plugin in admin panel
 */
//add_action('alert', 'fun_alert');
add_action('admin_table', 'fun_admin_table');
function fun_admin_table()
{
    global $wpdb, $tableOpenDoor;
    $openDoor = get_opendoor();
    $idOpenDoor = array();
    foreach ($openDoor as $key => $value) {
        //echo "$value->id_calendar<br>";
        $idOpenDoor[] = $value->id_calendar;
    }
    $tableCalendars = $wpdb->prefix . "dopbsp_calendars";
    $query = "SELECT id , name FROM $tableCalendars";
    $results = $wpdb->get_results($query);
    $idCalendar = array();
    foreach ($results as $key => $value) {
        $idCalendar[] = $value->id;
        if (!in_array($value->id, $idOpenDoor)) {
            $query = "INSERT INTO $tableOpenDoor(id_calendar,name_calendar ) VALUES({$value->id}, '{$value->name}')";
            $wpdb->get_results($query);
        }
    }
    foreach ($idOpenDoor as $key=>$value) {
        if (!in_array($value, $idCalendar)) {
            echo $value . '<br>';
            $query = "DELETE FROM $tableOpenDoor WHERE id_calendar=$value";
            $wpdb->get_results($query);
        }
    }
}


/**
 * the choosing of time from'dopbsp_reservations' by 'id' of the paid orders from 'postmeta'
 * choose all orders of the particular user and check the nearest order(time)
 * @param int - id of user who is trying to authorize
 * @return bool
 */
function get_order_reservation($idUser){
    global $wpdb;
    $tableReservations = $wpdb->prefix . "dopbsp_reservations";
    $query = "
    SELECT calendar_id, check_in , start_hour , transaction_id 
    FROM $tableReservations
    WHERE transaction_id IN(
        SELECT post_id FROM {$wpdb->postmeta} 
        WHERE post_id IN (
            SELECT post_id FROM {$wpdb->postmeta}
            WHERE meta_key = '_customer_user'
            AND meta_value = $idUser ) 
        AND meta_key = '_date_completed' 
        AND meta_value != '' ) GROUP BY transaction_id ORDER BY check_in , start_hour";
    $results = $wpdb->get_results($query);

    foreach ($results as $key => $value) {
        // accurate time
        $timeEvent = $value->check_in . ' ' . $value->start_hour;
        $exactTime = strtotime($timeEvent);

        //time before the beginning of event,hours, minutes
        $dt_time = get_time_before_start($value->calendar_id);
        $dt = $dt_time[0] * 3600 + $dt_time[1] * 60;
        $spanTime = $exactTime - $dt;

        if ($spanTime > current_time('timestamp')) {
            return 0;            

        } elseif ($exactTime > current_time('timestamp') && $spanTime < current_time('timestamp')) {
            return 1;
        } else {
            continue;
        }
    }
}



/**
 * the output of data from the table in database'opendoor'
 * @return - mixed the table of the plugin
 */
function get_opendoor(){
    global $wpdb;
    $tableOpenDoor = $wpdb->prefix . "opendoor";
    $query = "SELECT * FROM $tableOpenDoor";
    $results = $wpdb->get_results( $query );
    return $results;
}

/**
 * updating time before the beginning of event in the table of database'opendoor'
 * @param $getOpendoor -  existing table of the plugin
 */
function updata_time_before_start($getOpendoor){
    global $wpdb, $tableOpenDoor;
        foreach($getOpendoor as $key=>$value){
            //echo $value->id_calendar .'<br>';
            $query = 'UPDATE ' . $tableOpenDoor . '
            SET hours=' . $_POST["od-hours-$value->id_calendar"] . ' , minutes=' . $_POST["od-minutes-$value->id_calendar"] .'
            WHERE id_calendar=' .$value->id_calendar ;
            $wpdb->get_results($query);
        }
        //echo '<pre>';
        //var_dump($_POST);
        //echo '</pre>';
}


/**
 * the outputting of time before the beginning of event from the table'opendoor' in database
 * @param $calendarId - id of the calendar
 * @return array - in which time hrs:min for sent calendar 
 */
function get_time_before_start($calendarId){
    global $wpdb, $tableOpenDoor;

    //time before the beginning of event hours,minutes
    $dt_time = array();
    $query = "SELECT hours, minutes FROM $tableOpenDoor WHERE id_calendar =" . $calendarId;
    foreach($wpdb->get_results($query) as $key_1=>$value_1) {
        $dt_time[0] = $value_1->hours;
        $dt_time[1] = $value_1->minutes;
    }
    return $dt_time;
}


/**
 * checking log in and password of the user in the form of authorization
 * Checking data which were received when the form was sent 
 * @return int|string if the checking is done then it returns the function "get_order_reservation" с "id" of the logged in user,
 * if the checking hasn't done it returns the line with an error 
 *
 */
function open_door_open(){
    if(isset($_POST['od-form-open-sub'])) {
        if (isset($_POST['od-form-open-name']) && isset($_POST['od-form-open-pass'])) {

            /**
             * checking while authorization of the outputting  of the existing email
             */
            $userId = NULL;
            if (is_email($_POST['od-form-open-name'])) {
                $user = get_user_by('email', $_POST['od-form-open-name']);
                $userId = $user->id;
            }
            /**
             * the checking of the output of the existing log in in authorization
             */
            else {
                $user = get_user_by('login', $_POST['od-form-open-name']);
                $userId = $user->id;
            }
            if ($userId) {
                $user = get_userdata($userId);
                if ($user) {
                    /**
                     * checking of password
                     */
                    $password = $_POST['od-form-open-pass'];
                    $hash = $user->data->user_pass;
                    if (wp_check_password($password, $hash)) {

                        if(get_order_reservation($userId)) {
                            //var_dump(get_order_reservation($userId));
                            return get_order_reservation($userId);
                        }else {
                            //var_dump('Not time or no events');
                            return 'Not time or no events';
                        }
                    } else {
                        //var_dump('Invalid username or password');
                        return 'Invalid login or password';
                    }
                }
            }
        }
    }
    return 0;
}